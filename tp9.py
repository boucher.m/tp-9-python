"""Init Dev : TP9"""


# ==========================
# Petites bêtes
# ==========================

def toutes_les_familles(pokedex):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    familles = set()
    for (_, famille) in pokedex: # O(N)
        familles.add(famille) # O(1)
    return familles 

# complexité O(N)


def nombre_pokemons(pokedex, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    total = 0
    for (_, type) in pokedex: # O(N)
        if famille == type:  # O(1)
            total += 1  # O(1)
    return total

# complexité O(N)



def frequences_famille(pokedex):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str)
        et la valeur associée est le nombre de représentants de la famille (int)
    """
    mon_pokedex = {}
    for (_, famille) in pokedex: # O(N)
        if famille in mon_pokedex.keys(): # O(1)
            mon_pokedex[famille] += 1 # O(1)
        else:
            mon_pokedex[famille] = 1 # O(1)
    return mon_pokedex

# complexité O(N)


def dico_par_famille(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de cette
    famille dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    mon_pokedex = {}
    for (pokemon, famille) in pokedex: # O(N)
        if famille not in mon_pokedex.keys(): # O(1)
            mon_pokedex[famille] = {pokemon} # O(1)
        elif famille in mon_pokedex.keys(): # O(1)
            mon_pokedex[famille].add(pokemon) # O(1)
    return mon_pokedex

# complexité O(N)


def famille_la_plus_representee(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (list): liste de pokemon, chaque pokemon est modélisé par
        un couple de str (nom, famille)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    familles = frequences_famille(pokedex)
    plus_representee = None
    total = 0
    for (famille, nombre) in familles.items(): # O(N)
        if total < nombre: # O(1)
            total = nombre # O(1)
            plus_representee = famille # O(1)
    return plus_representee

# complexité O(N)

# ==========================
# La maison qui rend fou
# ==========================

def quel_guichet(mqrf:dict, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        str: le nom du guichet qui finit par donner le formulaire A-38
    """
    while mqrf[guichet] is not None:
        guichet = mqrf[guichet]
    return guichet


def quel_guichet_v2(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
    """
    total = 1
    while mqrf[guichet] is not None:
        guichet = mqrf[guichet]
        total += 1
    return (guichet, total)

def quel_guichet_v3(mqrf, guichet):
    """Détermine le nom du guichet qui délivre le formulaire A-38
    ainsi que le nombre de guichets visités

    Args:
        mqrf (dict): représente une maison qui rend fou
        guichet (str): le nom du guichet de départ qui est le nom d'un guichet de la mqrf

    Returns:
        tuple: le nom du guichet qui finit par donner le formulaire A-38 et le nombre de
        guichets visités pour y parvenir
        S'il n'est pas possible d'obtenir le formulaire en partant du guichet de depart,
        cette fonction renvoie None
    """
    total = 1
    while mqrf[guichet] is not None:
        guichet = mqrf[guichet]
        total += 1
        if total > len(mqrf):
            return None

    return (guichet, total)


# ==========================
# Petites bêtes (la suite)
# ==========================


def toutes_les_familles_v2(pokedex:dict):
    """détermine l'ensemble des familles représentées dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        set: l'ensemble des familles représentées dans le pokedex
    """
    familles = set()
    for famille in pokedex.values():
        for type in famille:
            familles.add(type) 
    return familles

def nombre_pokemons_v2(pokedex:dict, famille):
    """calcule le nombre de pokemons d'une certaine famille dans un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)
        famille (str): le nom de la famille concernée

    Returns:
        int: le nombre de pokemons d'une certaine famille dans un pokedex
    """
    total = 0
    for type in pokedex.values(): 
        if famille in type:  
            total += 1  
    return total

def frequences_famille_v2(pokedex:dict):
    """Construit le dictionnaire de fréqeunces des familles d'un pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur
        associée est le nombre de représentants de la famille (int)
    """
    mon_pokedex = {}
    for famille in pokedex.values(): 
        for type in famille:
            if type in mon_pokedex.keys(): 
                mon_pokedex[type] += 1 
            else:
                mon_pokedex[type] = 1 
    return mon_pokedex

def dico_par_famille_v2(pokedex):
    """Construit un dictionnaire dont les les clés sont le nom de familles (str)
    et la valeur associée est l'ensemble (set) des noms des pokemons de
    cette famille dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        dict: un dictionnaire dont les clés sont le nom de familles (str) et la valeur associée est
        l'ensemble (set) des noms des pokemons de cette famille dans le pokedex
    """
    ...

def famille_la_plus_representee_v2(pokedex):
    """détermine le nom de la famille la plus représentée dans le pokedex

    Args:
        pokedex (dict): un dictionnaire dont les clés sont les noms de pokemons et la
        valeur associée l'ensemble (set) de ses familles (str)

    Returns:
        str: le nom de la famille la plus représentée dans le pokedex
    """
    ...
